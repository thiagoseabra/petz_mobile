import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import java.awt.event.KeyEvent
import java.util.concurrent.ConcurrentHashMap.KeySetView

import javax.swing.JOptionPane

import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import io.appium.java_client.android.AndroidDriver
import io.appium.java_client.android.AndroidKeyCode

ArrayList<String> listProdutos = new ArrayList<String>()


Produto Racao1 = new Produto("Ração Fórmula Golden para Cães Adultos sabor Peru e Arroz 15kg")
listProdutos.add(Racao1)
Produto Racao2 = new Produto("Ração Golden Fórmula Light para Cães Adultos - 15kg")
listProdutos.add(Racao2)
Produto Racao3 = new Produto("Caneca Dosadora Petz para Cães e Gatos Azul")
listProdutos.add(Racao3)
Produto Racao4 = new Produto("Ração Premier Raças Específicas Golden Retriever para Cães Adultos - 12kg")
listProdutos.add(Racao4)
Produto Racao5 = new Produto("Ração Golden Power Training para Cães Adultos Sabor Frango e Arroz - 15kg")
listProdutos.add(Racao5)

Mobile.startExistingApplication("br.com.petz")

Mobile.tap( findTestObject('Object Repository/Petz/edt_Search') , 0)

for(i=0; i<listProdutos.size(); i++)
{

Produto p = listProdutos.get(i)

Mobile.tap( findTestObject('Object Repository/Petz/racao') , 0)

Mobile.scrollToText(p.getNome())

CustomKeywords.'Utils.TapText'(p.getNome())

p.setValorNormal(Mobile.getText(  findTestObject('Object Repository/Petz/txt_PrecoNormal'), 0))

p.setValorAssinante(Mobile.getText( findTestObject('Object Repository/Petz/txt_PrecoAssinante') , 0))

Mobile.tap(  findTestObject('Object Repository/Petz/btn_AdicionarAoCarinho'), 0)

Mobile.tap( findTestObject('Object Repository/Petz/btn_IrAoCarrinho') , 0)

if(!p.getNome().equalsIgnoreCase(Mobile.getText( findTestObject('Object Repository/Petz/txt_NomeProdCarrinho') , 0)))
{
	JOptionPane.showMessageDialog(null, "Houve divergencia nos nomes do Produto\nNome Esperado:"+p.getNome()+"\nNome no Carrinho:" + Mobile.getText( findTestObject('Object Repository/Petz/txt_NomeProdCarrinho') , 0))
	Mobile.closeApplication()
	throw new Error();
}

if(!p.getValorNormal().equalsIgnoreCase(Mobile.getText( findTestObject('Object Repository/Petz/txt_PrecoCarrinho') , 0)))
{
	JOptionPane.showMessageDialog(null, "Houve divergencia nos nomes do Produto\nNome Esperado:"+p.getValorNormal()+"\nNome no Carrinho:" + Mobile.getText( findTestObject('Object Repository/Petz/txt_PrecoCarrinho') , 0))
	Mobile.closeApplication()
	throw new Error();
}

Mobile.tap( findTestObject('Object Repository/Petz/btn_DeletaProdCarrinho') , 0)

Mobile.tap( findTestObject('Object Repository/Petz/btn_Sim') , 0)

Mobile.tap( findTestObject('Object Repository/Petz/btn_Voltar') , 0)

Mobile.tap( findTestObject('Object Repository/Petz/btn_Buscar') , 0)
}
