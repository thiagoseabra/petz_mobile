import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import java.awt.event.KeyEvent
import java.util.concurrent.ConcurrentHashMap.KeySetView

import javax.swing.JOptionPane

import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import io.appium.java_client.android.AndroidDriver
import io.appium.java_client.android.AndroidKeyCode

Mobile.startExistingApplication("br.com.petz")

// Mobile.tap( findTestObject('Petz/btn_ComecarSemLogin') , 0, FailureHandling.STOP_ON_FAILURE)

Mobile.tap( findTestObject('Object Repository/Petz/edt_Search') , 0)

Mobile.tap( findTestObject('Object Repository/Petz/racao') , 0)

Mobile.tap( findTestObject('Object Repository/Petz/Produto_3') , 0)

String ProdutoNome = Mobile.getText( findTestObject('Object Repository/Petz/txt_NomeProduto') , 0)

String ValorProd = Mobile.getText(  findTestObject('Object Repository/Petz/txt_PrecoNormal'), 0)

String ValorAssinante = Mobile.getText( findTestObject('Object Repository/Petz/txt_PrecoAssinante') , 0)

Mobile.tap(  findTestObject('Object Repository/Petz/btn_AdicionarAoCarinho'), 0)

Mobile.tap( findTestObject('Object Repository/Petz/btn_IrAoCarrinho') , 0)

if(!ProdutoNome.equalsIgnoreCase(Mobile.getText( findTestObject('Object Repository/Petz/txt_NomeProdCarrinho') , 0)))
{
	JOptionPane.showMessageDialog(null, "Houve divergencia nos nomes do Produto\nNome Esperado:"+ProdutoNome+"\nNome no Carrinho:" + Mobile.getText( findTestObject('Object Repository/Petz/txt_NomeProdCarrinho') , 0))
	Mobile.closeApplication()
	throw new Error();
}

if(!ValorProd.equalsIgnoreCase(Mobile.getText( findTestObject('Object Repository/Petz/txt_PrecoCarrinho') , 0)))
{
	JOptionPane.showMessageDialog(null, "Houve divergencia nos nomes do Produto\nNome Esperado:"+ProdutoNome+"\nNome no Carrinho:" + Mobile.getText( findTestObject('Object Repository/Petz/txt_PrecoCarrinho') , 0))
	Mobile.closeApplication()
	throw new Error();
}

Mobile.closeApplication()

